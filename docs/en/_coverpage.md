<!-- ![logo](_media/icon.svg) -->

# [fastposter <small>1.4.3</small>]()

> 一个神奇的海报生成器

- 简单、易用、快速、强大
- 无需编写复杂的绘图代码
- 经历众多电商级生产环境考验
- 众多语言支持 Java Python PHP JS 等

[GitHub](https://github.com/psoho/fast-poster)
[Gitee](https://gitee.com/psoho/fast-poster)
[快速开始](#quick-start)

<!-- 背景图片 -->

<!-- ![](_media/bg.png) -->

<!-- 背景色 -->

<!-- ![color](#e3eaef) -->